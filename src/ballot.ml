(*
    threshold de validité
    threshold de fin
    threshold d'invalidité

    particularité du vote binaire (pour définir une option avec un seuil spécial) ou juste nom d'option

    choix avec veto: InvalidityThreshold (SpecialOption ('non', Absolute 1))
    choix avec veto: InvalidityThreshold (Participation (Relative 0.8))

    - existence d'un buffer de scrutins proposés mais pas encore acceptés?
    use case: on ne veut pas que tout le monde propose des scrutins pour kicker quelqu'un dès qu'ils ne sont pas contents
    donc on veut qu'un vote soit déclenché à partir d'un certain seuil
    => proposition : le starting_threshold ne définit pas le seuil de création du scrutin mais son seuil de visibilité
       => tout le monde peut créer un scrutin, il est seulement invisible au départ



    process:
      règle de
*)

(* workaround for annoying bug in LSP *)
type a_string = string

type an_integer = int

type choice = string

type range_start = int

type range_end = int

type power_level = int

type duration = Month of int | Week of int | Day of int | Hour of int

type threshold_qty = Absolute of int | Relative of float

type threshold_category =
  | InvalidityThreshold
  | ValidityThreshold
  | StartingThreshold
  | EndingThreshold
  | WinningThreshold

type threshold_criterium =
  | Participation of threshold_qty
  | SpecialChoice of choice * threshold_qty
  | Duration of duration

type threshold = threshold_category * threshold_criterium

(* type threshold = threshold_index * threshold_category * threshold_criterium *)

(* ; winning_threshold: (choice * threshold_qty) list *)
(* winning_threshold can only be made of "SpecialChoice thresholds" *)

type ballot_type =
  | SingleChoice
  | OrderedChoice
  | WeightedChoice of range_start * range_end

type ballot_rule =
  {ballot_type: ballot_type; thresholds: threshold list; choices: choice list}

type democracy_rule = Ballot of ballot_rule | MinimumPowerLevel of power_level

type rule = {target: string; process: democracy_rule}

type rule_list = rule list

let string_of_duration = function
  | Month i ->
      string_of_int i ^ " months"
  | Week i ->
      string_of_int i ^ " weeks"
  | Day i ->
      string_of_int i ^ " days"
  | Hour i ->
      string_of_int i ^ " hours"

let duration_of_int i = Hour i

let int_of_duration = function
  | Month i ->
      i * 3600 * 24 * 7 * 30
  | Week i ->
      i * 3600 * 24 * 7
  | Day i ->
      i * 3600 * 24
  | Hour i ->
      i * 3600

let string_of_threshold_category = function
  | StartingThreshold ->
      "condition to start"
  | EndingThreshold ->
      "triggers the end"
  | ValidityThreshold ->
      "validity threshold"
  | InvalidityThreshold ->
      "triggers invalidity"
  | WinningThreshold ->
      "condition for winning"

let category_string_of_threshold (category, _criterium) =
  string_of_threshold_category category

let max_amount qty_amount = max (qty_amount 0)

let min_amount = max 0.

let max_amount = min 1.

let min_int_amount = max 0

let validated_threshold_qty unverified_qty =
  match unverified_qty with
  | Absolute qty_amount ->
      Absolute (min_int_amount qty_amount)
  | Relative input ->
      Relative (min_amount (max_amount input))

let compute_threshold_qty (old_qty_amount, qty_amount_string) =
  (* TODO: use opt? *)
  match (old_qty_amount, qty_amount_string) with
  | Absolute _, input ->
      (* Absolute (int_of_string_opt input) *)
      Absolute (int_of_string input) |> validated_threshold_qty
  | Relative _, input ->
      (* Relative (float_of_string_opt input) *)
      Relative (0.01 *. float_of_string input) |> validated_threshold_qty

let int_of_threshold_qty_amount = function
  | Absolute i ->
      i
  | Relative f ->
      int_of_float (f *. 100.0)

let string_of_threshold_qty = function
  | Absolute 1 ->
      "1 vote"
  | Absolute i ->
      string_of_int i ^ " votes"
  | Relative f ->
      string_of_int (int_of_threshold_qty_amount (Relative f)) ^ "% of votes"

let string_of_threshold_criterium = function
  | Participation _qty ->
      "participation"
  | SpecialChoice (c, _qty) ->
      "choice: " ^ c
  | Duration _d ->
      "duration"

let criterium_string_of_threshold (_category, criterium) =
  string_of_threshold_criterium criterium

let threshold_qty_string_of_threshold_criterium = function
  | Participation qty ->
      string_of_threshold_qty qty
  | SpecialChoice (_c, qty) ->
      string_of_threshold_qty qty
  | Duration d ->
      string_of_duration d

let string_of_threshold (category, condition) =
  "("
  ^ string_of_threshold_category category
  ^ ") "
  ^ string_of_threshold_criterium condition

let string_of_ballot_type = function
  | SingleChoice ->
      "Single choice"
  | OrderedChoice ->
      "Ordered choice"
  | WeightedChoice (a, b) ->
      "Weighted choice between " ^ string_of_int a ^ " and " ^ string_of_int b

let example_rules =
  [ { target= "m.room.kick"
    ; process=
        Ballot
          { ballot_type= SingleChoice
          ; choices= ["yes"; "no"]
          ; thresholds=
              [ (StartingThreshold, Participation (Absolute 3))
              ; (* quorum *)
                (ValidityThreshold, Participation (Relative 0.6))
              ; (EndingThreshold, Duration (Week 1))
              ; (* (InvalidityThreshold, SpecialChoice ("no", Absolute)) *)
                (* can there be 2 "winners"? *)
                (WinningThreshold, SpecialChoice ("yes", Relative 0.66)) ] } }
  ]
