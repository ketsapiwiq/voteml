open Ballot

type model =
  { ballot_rule: ballot_rule
  ; new_choice: choice
  ; current_threshold: threshold
  ; new_special_choice: string }

type msg =
  | SaveBallotType of ballot_type
  | AddChoice
  | SetNewChoice of choice
  | RemoveChoice of choice
  | AddThreshold
  (* fired by an input aiming at converting relative to absolute and back *)
  | SetThresholdQty of threshold_qty
  (* we cannot type the amount further as that would require to patch melange w/ `input let onInput ?(key="range")` *)
  | SetThresholdQtyAmount of string
  | SetCurrentThreshold of threshold
  | RemoveThreshold of threshold
  | SetThresholdCategory of threshold_category
  | SetCriterium of threshold_criterium
  | SetSpecialChoice of choice
[@@bs.deriving {accessors}]

let msg_to_string = function
  | SaveBallotType ballot_type ->
      string_of_ballot_type ballot_type
  | AddChoice ->
      "adding choice"
  | SetThresholdQty qty ->
      string_of_threshold_qty qty
  | SetNewChoice _choice ->
      "setting new choice"
  | SetSpecialChoice _choice ->
      "setting new choice"
  | RemoveChoice _choice ->
      "removing choice"
  | AddThreshold ->
      "AddThreshold"
  | RemoveThreshold _ ->
      "RemoveThreshold"
  | SetThresholdCategory category ->
      string_of_threshold_category category
  | SetCriterium criterium ->
      string_of_threshold_criterium criterium
  | SetThresholdQtyAmount _ ->
      "SetThresholdQtyAmount"
  | SetCurrentThreshold _ ->
      "SetCurrentThreshold"

let update (model : model) = function
  | SaveBallotType ballot_type ->
      let ballot_rule = {model.ballot_rule with ballot_type} in
      ({model with ballot_rule}, Tea.Cmd.none)
  | SetNewChoice new_choice ->
      ({model with new_choice}, Tea.Cmd.none)
  | SetCurrentThreshold current_threshold ->
      ({model with current_threshold}, Tea.Cmd.none)
  | AddThreshold ->
      let thresholds =
        if
          Belt.List.has model.ballot_rule.thresholds model.current_threshold
            ( = )
        then model.ballot_rule.thresholds
        else List.append model.ballot_rule.thresholds [model.current_threshold]
      in
      let ballot_rule = {model.ballot_rule with thresholds} in
      ({model with ballot_rule; new_choice= ""}, Tea.Cmd.none)
  | RemoveThreshold threshold ->
      let thresholds =
        Belt.List.keep model.ballot_rule.thresholds (fun n -> n <> threshold)
      in
      let ballot_rule = {model.ballot_rule with thresholds} in
      ({model with ballot_rule}, Tea.Cmd.none)
  | SetThresholdQty new_threshold_qty ->
      let current_threshold =
        match model.current_threshold with
        | category, Participation _old_qty ->
            (category, Participation new_threshold_qty)
        | category, SpecialChoice (choice, _old_qty) ->
            (category, SpecialChoice (choice, new_threshold_qty))
        | category, Duration d ->
            (category, Duration d)
      in
      ({model with current_threshold}, Tea.Cmd.none)
  | SetThresholdQtyAmount string_of_threshold_qty_amount ->
      let current_threshold =
        match model.current_threshold with
        | category, Participation old_qty ->
            ( category
            , Participation
                (compute_threshold_qty
                   (old_qty, string_of_threshold_qty_amount) ) )
        | category, SpecialChoice (choice, old_qty) ->
            ( category
            , SpecialChoice
                ( choice
                , compute_threshold_qty (old_qty, string_of_threshold_qty_amount)
                ) )
        | category, Duration d ->
            (category, Duration d)
      in
      ({model with current_threshold}, Tea.Cmd.none)
  | AddChoice ->
      let choices =
        if Belt.List.has model.ballot_rule.choices model.new_choice ( = ) then
          model.ballot_rule.choices
        else List.append model.ballot_rule.choices [model.new_choice]
      in
      let ballot_rule = {model.ballot_rule with choices} in
      ({model with ballot_rule; new_choice= ""}, Tea.Cmd.none)
  | RemoveChoice choice ->
      let choices =
        Belt.List.keep model.ballot_rule.choices (fun n -> n <> choice)
      in
      let ballot_rule = {model.ballot_rule with choices} in
      ({model with ballot_rule}, Tea.Cmd.none)
  | SetThresholdCategory category ->
      let criterium =
        match model.current_threshold with _, criterium -> criterium
      in
      ({model with current_threshold= (category, criterium)}, Tea.Cmd.none)
  | SetSpecialChoice new_special_choice ->
      ({model with new_special_choice}, Tea.Cmd.none)
  | SetCriterium criterium ->
      let current_threshold =
        match model.current_threshold with category, _ -> (category, criterium)
      in
      ({model with current_threshold}, Tea.Cmd.none)

let init () =
  let ballot_rule : Ballot.ballot_rule =
    { ballot_type= SingleChoice
    ; choices= ["yes"; "no"]
    ; thresholds=
        [ (StartingThreshold, Participation (Absolute 3))
        ; (ValidityThreshold, Participation (Relative 0.6))
        ; (EndingThreshold, Duration (Week 1))
        ; (WinningThreshold, SpecialChoice ("yes", Relative 0.66)) ] }
  in
  let model =
    { ballot_rule
    ; new_choice= ""
    ; new_special_choice= ""
    ; current_threshold= (StartingThreshold, Participation (Absolute 1)) }
  in
  (model, Tea.Cmd.none)

let view model =
  let open Tea.Html in
  let open Tea.Html2 in
  let displayChoice choice =
    li
      [class' "mt-2"]
      [ button
          [class' "btn gap-2"]
          [ text choice
          ; div
              [class' "badge badge-secondary"; onClick (removeChoice choice)]
              [text "X"] ] ]
  in
  let displayChoices =
    fieldset
      [class' "card card-bordered"]
      [ legend [] [text "Choices:"]
      ; div
          [class' "card-body"]
          [ ul [] (List.map displayChoice model.ballot_rule.choices)
          ; label
              [class' "input-group"]
              [ input'
                  [ type' "text"
                  ; class' "input input-bordered"
                  ; id "new_choice"
                  ; name "new_choice"
                  ; placeholder "Add an option"
                  ; onInput setNewChoice
                  ; value model.new_choice ]
                  []
              ; button [class' "btn btn-square"; onClick addChoice] [text "add"]
              ] ] ]
  in
  let dropdownInput (dropdown_id, current_text, dropdownOptions) =
    div
      [class' "dropdown"; id dropdown_id]
      [ label [Attributes.tabindex 0; class' "btn"] [text current_text]
      ; ul [Attributes.tabindex 0; class' "dropdown-content"] dropdownOptions ]
  in
  let dropdownOption msg =
    li [class' ""] [a [onClick msg] [text (msg_to_string msg)]]
  in
  let editBallotType ballot_type =
    dropdownInput
      ( "edit-ballot-type"
      , string_of_ballot_type ballot_type
      , [ dropdownOption (SaveBallotType SingleChoice)
        ; dropdownOption (SaveBallotType OrderedChoice) ] )
  in
  let displayThreshold (threshold_category, threshold_criterium) =
    li
      [class' "mt-2"]
      [ div
          [class' "threshold-box"]
          [ div
              [class' "stat"]
              [ span
                  [class' "stat-title"]
                  [text (string_of_threshold_category threshold_category)]
              ; span
                  [class' "stat-value"]
                  [ text
                      (threshold_qty_string_of_threshold_criterium
                         threshold_criterium ) ]
              ; span
                  [class' "stat-desc"]
                  [ text
                      ( "("
                      ^ string_of_threshold_criterium threshold_criterium
                      ^ ")" ) ] ]
          ; label
          [ for' "edit-threshold-modal"
          ; class' "modal-button btn"
          ; onClick
              (SetCurrentThreshold (threshold_category, threshold_criterium))
          ]
          [text "Edit"] ;button
          [ class' "delete-threshold btn btn-square"
          ; onClick
              (RemoveThreshold (threshold_category, threshold_criterium))
          ]
          [text "X"] ] ]
  in
  let filterThresholds category =
    List.filter (function cat, _ -> cat = category)
  in
  let listCategoryThresholds (threshold_category, title) =
    div []
      [ div [class' "divider mx-64"] [text title]
      ; ul
          [class' "columns-sm gap-8"]
          (List.map displayThreshold
             (filterThresholds threshold_category model.ballot_rule.thresholds) )
      ]
  in
  let listAllThresholds =
    div
      [class' ""; id "existing-thresholds"]
      [ label
          [for' "edit-threshold-modal"; class' "btn modal-button w-full"]
          [text "New threshold"]
      ; listCategoryThresholds (StartingThreshold, "Start")
      ; listCategoryThresholds (EndingThreshold, "End")
      ; listCategoryThresholds (ValidityThreshold, "Valid if?")
      ; listCategoryThresholds (InvalidityThreshold, "Invalid if?")
      ; listCategoryThresholds (WinningThreshold, "wins if...") ]
  in
  (* TODO: add 2 helper functions to convert nicely from percentage of ppl in room to number of ppl in room
     (let absolute_of ratio_ppl_in_the_room and let relative_of ppl_in_the_room) *)
  let qtyConstructorInput current_qty =
    dropdownInput
      ( "qty-constructor"
      , string_of_threshold_qty current_qty
      , [ dropdownOption (setThresholdQty (Absolute 1))
        ; dropdownOption (setThresholdQty (Relative 0.5)) ] )
  in
  let participationInput qty =
    div
      [id "participation-input"]
      [ ( match qty with
        | Absolute qty_amount ->
            div [class' ""][div [class' "btn no-animation"][text "How many people?"]
            ;input'
              [ type' "number"
              ; class' "input input-bordered";
               Attributes.min("0")
              ; value (string_of_int qty_amount) ]
              []]
        | Relative qty_amount ->
            div []
              [ input'
                  [ type' "range"
                  ; class' "range"
                  ; onInput setThresholdQtyAmount
                  ; value (string_of_float (Belt.Float.( * ) qty_amount 100.))
                  ]
                  []
              ; div
                  [class' ""]
                  [text (string_of_threshold_qty (Relative qty_amount))] ] ) ]
  in
  let durationInput duration =
    (* = (match criterium with
       | Duration (Day i) ->
           div [] [input' [type' "date"; value (string_of_int i)] []; select [] []]
       | Duration (Hour i) ->
           div [] [input' [type' "time"; value (string_of_int i)] []; select [] []]
       | Duration (Week i) ->
           div [] [input' [type' "date"; value (string_of_int i)] []; select [] []]
       | Duration (Month i) ->
           div [] [input' [type' "date"; value (string_of_int i)] []; select [] []] *)
    input'
      [ type' "date"
      ; onInput setThresholdQtyAmount
      ; class' "input input-bordered"
      ; value (string_of_duration duration) ]
      []
  in
  let first_choice =
    match Belt.List.head model.ballot_rule.choices with
    | Some choice ->
        choice
    | None ->
        ""
  in
  let special_choice_value =
    match model.new_special_choice with "" -> first_choice | choice -> choice
  in
  let specialChoiceInput (_preselected_choice, threshold_qty) =
    div []
      [ input'
          [ class' "input "
          ; type' "text"
          ; value special_choice_value
          ; onInput setSpecialChoice
          ; placeholder "the special choice" ]
          []
      ; participationInput threshold_qty ]
  in
  let criteriumInput =
    dropdownInput
      ( "criterium"
      , criterium_string_of_threshold model.current_threshold
      , [ dropdownOption (SetCriterium (Participation (Relative 0.5)))
        ; dropdownOption (SetCriterium (Duration (Week 1)))
        ; dropdownOption
            (SetCriterium (SpecialChoice (first_choice, Absolute 3)))
        ; dropdownOption (SetCriterium (Participation (Relative 0.5))) ] )
  in
  let thresholdForm (threshold_category, threshold_criterium) =
    div
      [class' "threshold-form"]
      [ div
          [class' ""]
          [ div
              [class' "threshold-inputs"]
              [ criteriumInput
              ; ( match threshold_criterium with
                | Participation qty ->
                    qtyConstructorInput qty
                | SpecialChoice (_choice, threshold_qty) ->
                    qtyConstructorInput threshold_qty
                | Duration _duration ->
                    div [] [] )
              ; ( match threshold_criterium with
                | Participation qty ->
                    participationInput qty
                | SpecialChoice (choice, threshold_qty) ->
                    specialChoiceInput (choice, threshold_qty)
                | Duration duration ->
                    durationInput duration )
              ; button [class' "btn"; onClick addThreshold] [text "Add"]
              ; label
                  [ for' "edit-threshold-modal";class' "btn"
                  ; onClick
                      (removeThreshold
                         (model.current_threshold) ) ]
                  [text "Delete"] ] ] ]
  in
  (* modal boilerplate *)
  let threshold_modal =
    (* let modal dom = *)
    div []
      [ input'
          [class' "modal-toggle"; id "edit-threshold-modal"; type' "checkbox"]
          []
      ; label
          [for' "edit-threshold-modal"; class' "modal cursor-pointer"]
          [ label
              [for' ""; class' "modal-box"]
              [ div
                  [class' ""]
                  [ h3 [class' "h3"] [text "Editing threshold!"]
                  ; div
                      [class' "card-body"]
                      [ div
                          [class' "threshold-inputs"]
                          [ dropdownInput
                              ( "category"
                              , category_string_of_threshold
                                  model.current_threshold
                              , [ dropdownOption
                                    (SetThresholdCategory ValidityThreshold)
                                ; dropdownOption
                                    (SetThresholdCategory InvalidityThreshold)
                                ; dropdownOption
                                    (SetThresholdCategory WinningThreshold)
                                ; dropdownOption
                                    (SetThresholdCategory StartingThreshold)
                                ; dropdownOption
                                    (SetThresholdCategory EndingThreshold) ] )
                          ; thresholdForm model.current_threshold
                          ; div [] [label [for' "edit-threshold-modal"; onClick addThreshold; class' "btn"] [text "Yay!"]] ] ] ] ] ] ]
  in
  let display_ballot =
    div
      [class' ""; id "display-ballot"]
      [ div [class' "card-title"] [text "Ballot \"Lorem\""]
      ; editBallotType model.ballot_rule.ballot_type
      ; displayChoices
      ; div [class' "divider"] []
      ; listAllThresholds ]
  in
  div
    [class' "container mt-8"]
    [ div
        [class' "cards"]
        [ div
            [class' "card card-bordered shadow-xl"]
            [ div
                [class' "card-body"]
                [ div [class' ""] [display_ballot]
                ; label
                    [for' "edit-threshold-modal"; class' "btn modal-button"]
                    [text "New threshold"]
                ; threshold_modal ] ] ] ]

let subscriptions _model = Tea.Sub.none

let shutdown _model = Tea.Cmd.none

let start_app container =
  Tea.App.standardProgram {init; update; view; subscriptions} container ()

let start_debug_app ?(init = init) ?(shutdown = shutdown) container =
  Tea.App.standardProgram {init; update; view; subscriptions} container ()

let start_hot_debug_app container cachedModel =
  (* Replace the existing shutdown function with one that returns the current
   * state of the app, for hot module replacement purposes *)
  (* inspired by https://github.com/walfie/ac-tune-maker *)
  let modelRef = ref None in
  let shutdown model =
    modelRef := Some model ;
    Tea.Cmd.none
  in
  let init =
    match cachedModel with
    | None ->
        init
    | Some model ->
        fun _flags ->
          let _model, cmd = init () in
          (model, cmd)
  in
  let app = start_debug_app ~init ~shutdown container in
  let oldShutdown = app##shutdown in
  let newShutdown () = oldShutdown () ; !modelRef in
  let _ = Js.Obj.assign app [%obj {shutdown= newShutdown}] in
  newShutdown
