external env : string = "import.meta.env.MODE" [@@bs.val]

external base_url : string = "import.meta.env.BASE_URL" [@@bs.val]

external is_prod : bool = "import.meta.env.PROD" [@@bs.val]

external is_dev : bool = "import.meta.env.DEV" [@@bs.val]
