module.exports = {
  content: ['./index.html',
   './src/**/*.ml',
    './src/**/*.{vue,js,ts}'
  ],
  darkMode: 'class',
  theme: {
      screens: {
        'tablet': '640px',
        // => @media (min-width: 640px) { ... }
  
        'laptop': '1024px',
        // => @media (min-width: 1024px) { ... }
  
        'desktop': '1280px',
        // => @media (min-width: 1280px) { ... }
      },
    container: {
      padding: '2rem'},
    extend: {},
    colors: {
      'danger-red': '#ed1515',
      'abyss-blue': '#2980b9',
      'alternate-gray': '#bdc3c7',
      'beware-orange': '#f67400',
      'burnt-charcoal': '#3b4045',
      'cardboard-gray': '#eff0f1',
      'charcoal-gray': '#31363b',
      'coastal-fog': '#7f8c8d',
      'deco-blue': '#1e92ff',
      'hover-blue': '#93cee9',
      'hyper-blue': '#3daee6',
      'icon-blue': '#1d99f3',
      'icon-gray': '#4d4d4d',
      'icon-green': '#2ecc71',
      'icon-red': '#804453',
      'icon-red': '#da4453',
      'icon-yellow': '#fdbc4b',
      'lazy-gray': '#afb0b3',
      'noble-fir': '#27ae60',
      'paper-white': '#fcfcfc',
      'pimpinella': '#e74c3c',
      'plasma-blue': '#3daee9',
      'shade-black': '#232629',
      'sunbeam-yellow': '#c9ce3b',
      'verdant-green': '#11d116',
    }
  },
  plugins: [require("@tailwindcss/typography"), require("daisyui")],}
